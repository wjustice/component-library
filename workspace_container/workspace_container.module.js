import workspaceContainerDirective from './workspace_container.directive';

export default angular.module('workspaceContainer', [])
    .directive('workspaceContainer', workspaceContainerDirective)
