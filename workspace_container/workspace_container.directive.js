// import template from "./workspace_container_directive.html!text";
import template from "./workspace_container.template.html!text";
import Packery from 'packery';
import Draggabilly from 'draggabilly';
import _ from 'lodash';
import Markit from '../quote_service/quote.service';
// import utils from 'my-utils';

var utils = {};

utils.addEvent = function(elem, event, fn) {
    if (elem.addEventListener) {
        elem.addEventListener(event, fn, false);
    } else {
        elem.attachEvent("on" + event, function() {
            // set the this pointer same as addEventListener when fn is called
            return (fn.call(elem, window.event));
        });
    }
}

function workspaceContainerDirective($timeout, $interval, $http, $window, $rootScope) {
    return {
        restrict: "E",
        template: template,
        link: function(scope, element) {
            var grid = {
                columnWidth: 100,
                rowHeight: 100
            }

            scope.grid = grid;

            var moduleDefaults = {
                list: {
                    w: 700,
                    h: 300
                },
                chart: {
                    w: 500,
                    h: 300
                },
                news: {
                    w: 400,
                    h: 400
                },
                orbits: {
                    w: 700,
                    h: 500
                },
                quote: {
                    w: 400,
                    h: 200
                }
            };

            // some starting modules
            scope.modules = [{
                type: 'list',
                h: 300,
                w: 800,
                minH: 300,
                minW: 700,
                scroll_offset: 0,
                data: []
            }, {
                symbol: 'MSFT',
                type: 'chart',
                h: 500,
                w: 700,
                minH: 300,
                minW: 700,
                scroll_offset: 0,
                data: []
            }, {
                type: 'list',
                h: 300,
                w: 800,
                minH: 300,
                minW: 700,
                scroll_offset: 0,
                data: []
            }];

            // A list of all the symbols in all the lists.  To be used in the randomized spoofing.
            let symbolStore = [];

            // A store off all quote data in all lists to be randomized.
            scope.masterDataObject = {};

            scope.myQuotelists = [{
                name: 'Quotelist 01',
                data: []
            }];

            let numberOfWatchlists = 7;

            class watchlist {
                constructor(properties) {
                    this.properties = properties;
                }

                toObject() {
                    return this.properties
                }
            }

            // create some starting watchlists
            scope.myWatchlists = _.times(numberOfWatchlists, x => new watchlist({
                name: 'Watchlist ' + (x + 1),
                data: []
            }).toObject())

            /*
             * A list of the data points from the quote data we actually want to display.
             * This also determines the table order when displayed:
             **/
            let headers = ['Sort', 'Symbol', 'Name', 'LastPrice', 'Change', 'ChangePercent', 'Volume', 'Bid', 'Ask', 'High', 'Low', 'Last', 'Open'];

            let roundToTwo = function(x) {
                return Math.round(x * 100) / 100;
            };

            let processData = function(object, index) {
                // round some numbers
                object = _.set(object, 'Change', roundToTwo(object.Change))
                object = _.set(object, 'ChangePercent', roundToTwo(object.ChangePercent))
                object = _.set(object, 'Bid', (object.High - 1.1))
                object = _.set(object, 'Ask', (object.Low + 2.3))
                object.Sort = index + 1;
                object.changed = false;

                // reduce the object to only the data points we want to show
                object = _.pick(object, headers)

                //push the symbol to the symbolStore
                symbolStore.push(object.Symbol);

                if (_.has(scope.masterDataObject, object.Symbol)) {

                } else {
                    scope.masterDataObject[object.Symbol] = object;
                }

                symbolStore = _.uniq(symbolStore);

                //we return just the symbol to be matched to the masterDataObject
                return object.Symbol;
            }

            $http.get('./data/watchlist.json').success(function(data) {
                // // reduce the object to only the data points we want to show
                // data = data.map(x => x = _.pick(x, headers))

                let processedData = [];
                let thisData = [];

                data.forEach((x, index) => processedData.push(processData(x, index)));
                processedData.forEach(x => thisData.push(scope.masterDataObject[x]))

                scope.myWatchlists[0].data = thisData;
                scope.myWatchlists.map(x => x.data = thisData)

                scope.modules[0].data = scope.myWatchlists[0].data;
                scope.modules[0].name = scope.myWatchlists[0].name;
            });

            $http.get('./data/quoteList.json').success(function(data) {
                let processedData = [];
                let thisData = [];

                data.forEach((x, index) => processedData.push(processData(x, index)));
                processedData.forEach(x => thisData.push(scope.masterDataObject[x]))

                scope.myQuotelists[0].data = thisData;
                scope.modules[2].data = scope.myQuotelists[0].data;
                scope.modules[2].name = scope.myQuotelists[0].name;
            });

            // You can us this to generate some starting data points to save to a json file.
            // let startingSymbols = ['AAPL', 'GE', 'NEE', 'MRKT', 'CAKE', 'ABEO', 'MU', 'GOOG', 'AXP', 'AVGO'];
            // let startingSymbols = ['GILD', 'JD', 'HIMX', 'VIPS', 'TSLA', 'MSFT', 'MCO', 'COKE', 'DVA', 'COST'];

            // startingSymbols.map(x => new Markit.QuoteService(x, function(jsonResult) {
            //     if (!jsonResult || jsonResult.Message) {
            //         console.error("Error: ", jsonResult.Message);
            //         return;
            //     }
            //     scope.myWatchlists[0].data.push(jsonResult);
            //     console.log(scope.myWatchlists[0].data);
            // }))


            function spoofData() {
                $interval(() => spoofTimer(_.random(2000, 5000)), 2000)
            }

            spoofData();

            let spoofTimer = function(delay) {
                let num = _.random(0, symbolStore.length - 1);
                let randomSymbol = symbolStore[num];
                let timer = $timeout(
                    function() {
                        let quote = scope.masterDataObject[randomSymbol];
                        if (quote === undefined) {
                            console.log(num, randomSymbol, symbolStore);
                        }
                        quote.ChangePercent = roundToTwo(quote.ChangePercent + (_.random(-.5, .5, true)));
                        quote.Bid = roundToTwo(quote.Bid + (_.random(-.5, .5, true)));
                        quote.Ask = roundToTwo(quote.Ask + (_.random(-.5, .5, true)));
                        quote.Change = roundToTwo(quote.Open * (quote.ChangePercent / 100));
                        quote.LastPrice = roundToTwo(quote.LastPrice + (quote.Change));

                        quote.changed = false;
                        quote.changed = true;
                        let classTimer = $timeout(
                            () => quote.changed = false, 1000
                        )
                        classTimer.then(
                            () => $timeout.cancel(classTimer), () => $timeout.cancel(classTimer)
                        );
                    },
                    delay
                );

                timer.then(
                    function() {
                        $timeout.cancel(timer);
                    },
                    function() {
                        // console.log("The public api probably timed out, sry :-/ --cancelling...", Date.now());
                        $timeout.cancel(timer);
                    }
                );
            }

            scope.$on('add-chart-from-list', function(event, type, data) {
                event.preventDefault();
                scope.addModule(type, null, null, data);
            });

            scope.$on('delete-module', function(event, module, index) {
                event.preventDefault();
                var theModule = workspaceContainer.find('.' + index);

                _.remove(scope.modules, {
                    $$hashKey: module.$$hashKey
                })

                pckry.remove(theModule[0]);
                pckry.layout()
            });

            scope.$on('add-list-module', function(event, list) {
                event.preventDefault();
                scope.addModule('list', list.data, list.name)
            });

            scope.$on('add-symbol-from-lookup', function(event, symbol, hashKey) {
                event.preventDefault();

                let module = _.find(scope.modules, {
                    $$hashKey: hashKey
                });

                var data = {};

                new Markit.QuoteService(symbol.Symbol, function(jsonResult) {
                    //Catch errors
                    if (!jsonResult || jsonResult.Message) {
                        console.error("Error: ", jsonResult.Message);
                        return;
                    }

                    //If all goes well, your quote will be here.
                    if (module.type == 'list') {
                        data = processData(jsonResult, 0);
                        data = scope.masterDataObject[data];
                        module.data.unshift(data)
                    } else {
                        module.symbol = symbol.Symbol;
                    }
                });
            });

            scope.$on('relayout', function(event, type, data) {
                event.preventDefault();
                pckry.layout();
            });

            var workspaceContainer = $('#container');

            //blank pckry on the root
            var pckry = {};

            //initialize packery on the next digest cycle
            $timeout(function() {
                initializePackery()
            }, 0);

            function initializePackery() {
                pckry = new Packery(workspaceContainer[0], {
                    // options
                    itemSelector: '.module',
                    columnWidth: grid.columnWidth,
                    rowHeight: grid.rowHeight
                });

                $timeout(function() {
                    workspaceContainer.find('.module').each(function(i, itemElem) {
                        // make element draggable with Draggabilly
                        var draggie = new Draggabilly(itemElem, {
                            handle: '.drag-handle'
                        });
                        // bind Draggabilly events to Packery
                        pckry.bindDraggabillyEvents(draggie);
                    });
                }, 0)
            }

            scope.addModule = function(type, data, name, symbol) {

                function Module(data) {
                    this.symbol = data.symbol;
                    this.type = data.type;
                    this.h = data.h;
                    this.w = data.w;
                    this.minH = data.minH;
                    this.minW = data.minW;
                    this.scroll_offset = 0;
                    this.data = data.data;
                    this.name = data.name;
                }

                if (!data) {
                    data = [];
                }
                if (!name && type === 'list') {
                    name = 'Quotelist ' + (scope.myQuotelists.length + 1);
                }

                var nm = {
                    type: type,
                    h: moduleDefaults[type].h,
                    w: moduleDefaults[type].w,
                    minH: moduleDefaults[type].h,
                    minW: moduleDefaults[type].w,
                    symbol: symbol,
                    data: data,
                    name: name
                };

                var newModule = new Module(nm);

                scope.modules.unshift(newModule);

                $timeout(function() {
                    // get the new module element, select by the class defined by index
                    var module = workspaceContainer.find('.module.0');

                    // add a drag handle, the above 'find' returns an array so we do module[0]
                    var draggie = new Draggabilly(module[0], {
                        handle: '.drag-handle'
                    });
                    pckry.bindDraggabillyEvents(draggie);

                    // trigger a re-layout
                    pckry.prepended(module);
                }, 10);
            }

            var thisWindow = angular.element($window);
            scope.getWindowDimensions = function() {
                return {
                    'h': thisWindow.height(),
                    'w': thisWindow.width()
                };
            };

            scope.$watch(scope.getWindowDimensions, function(newValue, oldValue) {
                if (newValue) {
                    scope.windowHeight = newValue.h;
                    scope.windowWidth = newValue.w;

                    $rootScope.$broadcast('window-resize', scope.windowWidth, scope.windowHeight);
                }
            }, true);

            $timeout(function() {
                thisWindow.bind('resize', function() {
                    scope.$apply();
                });
            }, 1000)
        }
    };
}

workspaceContainerDirective.$inject = ['$timeout', '$interval', '$http', '$window', '$rootScope']

export default workspaceContainerDirective;
