import template from "./orbits_directive.html!text";

// importing p5.js is not currently working so it's linked in the html at the moment.  Need to fix this.

function orbitsDirective($timeout) {
    return {
        restrict: "E",
        scope: {
            parent: '='
        },
        template: template,
        link: function(scope, element) {

                var sketch = function(p) {
                    var myGreen = [6, 247, 7],
                        myBlue = [63, 169, 245],
                        myRed = [255, 29, 37],
                        myOrange = [255, 147, 30],
                        myGray = [189, 204, 212],
                        myMutedGray = [65, 65, 65],
                        creamColor = [249, 242, 214],
                        // myBackground = [51, 51, 51];
                        myBackground = [38, 38, 38]

                    var colors = [myGreen, myBlue, myOrange, myRed, myGray];

                    var origin = {};
                    var orbits = [];
                    var moons = [1];
                    var lesser = 0;
                    var markerGroups = 0;

                    var parent = element[0];


                    var canvasWidth = scope.parent.w;
                    var canvasHeight = scope.parent.h;

                    var squares = [1, 2, 3, 4, 5]
                    var symbols = ['CAKE', 'TSLA', 'MRKT', 'GOOG', 'MU']

                    var redGreen = [myRed, myGreen];

                    var quotes = [];

                    var kuiperVolume = 400;

                    var error = false;

                    function Orbit(data, index) {
                        this.planet = {};
                        this.radius = {};

                        if (data != null) {
                            data.ChangePercentYTD = roundToTwo(data.ChangePercentYTD);
                            data.ChangePercent = roundToTwo(data.ChangePercent);

                            data.RelativeVolume = data.Volume / data.AverageVolume;

                            this.data = data;
                            this.planet.velocity = .3 * data.RelativeVolume; // needs to be normalized to the orbital width
                            this.symbol = data.Symbol;
                            this.planet.diameter = 65 * (data.MarketCap / largestMarketCap);
                            this.angle = data.ChangePercent;
                            // this.angle = getRandomInt(1, 360);
                            this.angle = 0;

                            // this.angle = (index + 1) * (630 / quotes.length)

                            this.radius.x = lesser / data.RelativeVolume;
                            this.radius.y = (this.radius.x) * (data.PE / data.Industry);
                            this.color = colors[index];
                            // console.log(data);
                        } else {
                            this.planet.diameter = getRandomInt(3, 5);
                            this.data = data;
                            this.angle = getRandomInt(0, 5);
                            this.radius.x = getRandomInt(lesser + 10, lesser + 120);
                            this.radius.y = getRandomInt(lesser + 10, lesser + 120);
                            this.planet.velocity = getRandomInt(.04, .41);
                            this.color = myMutedGray;
                            // this.color = redGreen[(getRandomInt(0, 2))]
                            this.color.push() // add opacity
                        }

                        this.theta = getRandomInt(0, 360);
                    }

                    var largestMarketCap = 725000000000;
                    var largestVolume = 0;

                    function createOrbits(data) {
                        // comment out to re-enable lookups
                        quotes = data;

                        var marketCaps = [];
                        var volumes = [];

                        // find the largest market cap
                        for (var i = data.length - 1; i >= 0; i--) {
                            marketCaps.push(data[i].MarketCap);
                            volumes.push(data[i].Volume);
                        };

                        // largestMarketCap = Math.max.apply(Math, marketCaps)
                        largestVolume = Math.max.apply(Math, volumes)

                        for (var i = quotes.length - 1; i >= 0; i--) {
                            var newOrbit = new Orbit(
                                quotes[i], // full quote data
                                i // index
                            );
                            orbits.push(newOrbit);
                        };

                        for (var i = kuiperVolume - 1; i >= 0; i--) {
                            newOrbit = new Orbit(
                                null, // null quote data
                                i //index
                            );
                            orbits.push(newOrbit);
                        };
                    }

                    p.setup = function() {
                        //uncomment to re-enable quote lookups:
                        // for (var i = symbols.length - 1; i >= 0; i--) {
                        //     getQuotes(symbols[i]);
                        // };

                        //comment out to re-enable quote lookups:
                        p.loadJSON('../data/quotes.json', createOrbits)

                        // var myCanvas = p.createCanvas(p.windowWidth, p.windowHeight);


                        var myCanvas = p.createCanvas(canvasWidth, canvasHeight);

                        myCanvas.parent(parent);

                        // origin.x = p.width / 2
                        // origin.y = p.height / 2

                        origin.x = scope.parent.w / 2
                        origin.y = scope.parent.h / 2

                        p.background(myBackground);
                        p.angleMode(p.DEGREES);

                        p.ellipseMode(p.RADIUS);
                        p.rectMode(p.RADIUS);

                        // lesser sets the max radius to the smaller of windowHeight/2 and windowWidth/2
                        // this keeps the orbits within the view of the window
                        if (origin.x >= origin.y) {
                            lesser = origin.y
                        } else {
                            lesser = origin.x
                        }

                        markerGroups = Math.floor(lesser / 65);

                    }

                    function getRandomInt(min, max) {
                        return Math.floor(Math.random() * (max - min)) + min;
                    }

                    function roundToTwo(num) {
                        return Math.round(num * 100) / 100
                    }

                    var count = 0;

                    function drawMarkers() {
                        // ring markers
                        for (var i = markerGroups - 1; i >= 0; i--) {
                            for (var j = 10 - 1; j >= 0; j--) {
                                var indent = 65 * i
                                p.push()
                                p.translate(origin.x, origin.y)
                                p.noFill()
                                p.stroke(45, 45, 45)
                                p.ellipse(0, 0, lesser - (j * 6) - indent, lesser - (j * 6) - indent)
                                p.pop()
                            };
                        };

                        // diamond marker
                        p.push()
                        p.noStroke()
                        p.translate(origin.x, origin.y)
                        p.rotate(45)
                        p.fill(47, 47, 47)
                        p.rect(0, 0, 10, 10)
                        p.pop()
                    }

                    function drawOrbits() {
                        for (var i = orbits.length - 1; i >= 0; i--) {
                            p.push()
                                // var a = orbits[i].angle * -5 // multiply by 10 to make the ChnagePercent more prevalent
                            var d = orbits[i].planet.diameter
                            var c = orbits[i].color
                            var r = orbits[i].radius

                            // normalizing semimajor and semiminor axis to get a visually accurate tilt:
                            if (r.x > r.y) {
                                var a = orbits[i].angle * -5
                                    // multiply by 10 to make the ChangePercent more prevalent
                            } else {
                                var a = orbits[i].angle * 5 // multiply by 10 to make the ChangePercent more prevalent
                            }

                            p.translate(origin.x, origin.y)

                            orbits[i].theta += orbits[i].planet.velocity

                            //http://math.stackexchange.com/questions/91132/how-to-get-the-limits-of-rotated-ellipse
                            var x0 = (r.x) * p.cos(orbits[i].theta)
                            var y0 = (r.y) * p.sin(orbits[i].theta)
                            var x2 = x0 * p.cos(a) - y0 * p.sin(a)
                            var y2 = x0 * p.sin(a) + y0 * p.cos(a)

                            if (orbits[i].data) {
                                p.stroke(myMutedGray)
                                p.noFill()
                                p.rotate(a)
                                p.ellipse(0, 0, r.x, r.y);

                                p.text(orbits[i].symbol, x2 + 4 + d, y2)
                                    // text('$' + orbits[i].data.LastPrice, x2 + 4 + d, y2 + 12)
                                p.text(orbits[i].data.ChangePercentYTD, x2 + 4 + d, y2 + 12)
                                p.text(orbits[i].data.ChangePercent, x2 + 4 + d, y2 + 24)
                                    // text(orbits[i].angle, x2 + 4 + d, y2 + 24)

                                // draw trails
                                var trailCount = 50;
                                for (var k = trailCount - 1; k >= 0; k--) {
                                    var step = (k * trailCount) / 60;
                                    var newTheta = orbits[i].theta - (step * orbits[i].planet.velocity)
                                    var x3 = (r.x) * p.cos(newTheta)
                                    var y3 = (r.y) * p.sin(newTheta)
                                    var x4 = x3 * p.cos(a) - y3 * p.sin(a)
                                    var y4 = x3 * p.sin(a) + y3 * p.cos(a)

                                    p.alpha(c)

                                    p.push()
                                    p.noStroke()
                                    p.fill(c)
                                    p.ellipse(x4, y4, 1, 1);
                                    p.pop()
                                };
                            }

                            p.push()
                            p.noStroke()
                            p.fill(c)
                            p.ellipse(x2, y2, d, d);

                            if (orbits[i].data) {
                                p.text(orbits[i].symbol, x2 + 4 + d, y2)
                                p.text(orbits[i].data.ChangePercentYTD, x2 + 4 + d, y2 + 12)
                                p.text(orbits[i].data.ChangePercent, x2 + 4 + d, y2 + 24)

                                for (var j = moons.length - 1; j >= 0; j--) {
                                    p.push()
                                    moons[j]
                                    p.noStroke()
                                    p.fill(creamColor)
                                    var x3 = x2 + ((i + 1) * 10 + d) * p.cos(count * (j + 1) * (i + 1))
                                    var y3 = y2 + ((i + 1) * 10 + d) * p.sin(count * (j + 1) * (i + 1))
                                    p.ellipse(x3, y3, 1, 1);
                                    p.pop()
                                };
                            };
                            p.pop()

                            p.pop()
                        };
                    }

                    function resizeEverything() {

                        origin.x = scope.parent.w / 2
                        origin.y = scope.parent.h / 2

                        if (origin.x >= origin.y) {
                            lesser = origin.y
                        } else {
                            lesser = origin.x
                        }

                        for (var i = orbits.length - 1; i >= 0; i--) {

                            if (orbits[i].data) {
                                orbits[i].radius.x = lesser / orbits[i].data.RelativeVolume;
                                orbits[i].radius.y = (orbits[i].radius.x) * (orbits[i].data.PE / orbits[i].data.Industry);
                            } else {
                                orbits[i].radius.x = getRandomInt(lesser + 10, lesser + 120);
                                orbits[i].radius.y = getRandomInt(lesser + 10, lesser + 120);
                            }
                        };

                        markerGroups = Math.floor(lesser / 65);

                        p.resizeCanvas(scope.parent.w, scope.parent.h)
                    }


                    scope.$watch('parent', function(newValue, oldValue) {
                        if (newValue) {
                            resizeEverything();
                        }
                    }, true);

                    p.draw = function() {
                        if (error) {
                            p.text('oops, an error occured', 25, 25)
                        }

                        // origin.x = p.width / 2
                        // origin.y = p.height / 2

                        origin.x = scope.parent.w / 2
                        origin.y = scope.parent.h / 2

                        element[0].offsetParent

                        p.background(myBackground);

                        if (count == 360) {
                            count = 0
                        } else {
                            count = count + 1
                        }

                        drawMarkers()
                        drawOrbits();
                    }
                }

                var myp5 = {};

                $timeout(function() {
                    myp5 = new p5(sketch);
                }, 0);

                scope.$on("$destroy", function() {
                    myp5.remove();
                });


            } // end link function
    };
}

orbitsDirective.$inject = ['$timeout']

export default orbitsDirective;
