import orbitsDirective from './orbits.directive';

export default angular.module('orbits', [])
    .directive('orbits', orbitsDirective)
