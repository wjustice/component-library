import template from "./markets_directive.html!text";

function marketsOverviewDirective($timeout, $interval, $window) {
    return {
        restrict: "E",
        template: template,
        link: function(scope, element) {

        }
    };
}

marketsOverviewDirective.$inject = ['$timeout', '$interval', '$window']

export default marketsOverviewDirective;
