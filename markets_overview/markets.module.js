import marketsOverviewDirective from './markets.directive';

export default angular.module('marketsOverview', [])
    .directive('marketsOverview', marketsOverviewDirective)
