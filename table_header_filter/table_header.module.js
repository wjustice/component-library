import tableHeaderFilter from './table_header.filter';

export default angular.module('tableHeader', [])
    .filter('tableHeader', tableHeaderFilter)
