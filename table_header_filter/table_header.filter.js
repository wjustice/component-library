function tableHeaderFilter() {
    // console.log('running tableheader filter');
    return function(input) {
        if (input == 'daychangedollar') {
            return 'Day Change ($)'
        } else if (input == 'lastprice') {
            return 'last price'
        } else if (input == 'previousclose') {
            return 'previous close'
        } else if (input == 'daychangepercent') {
            return 'day change (%)'
        } else if (input == 'daysvolume') {
            return 'days Volume'
        } else if (input == 'ChangePercent') {
            return 'Change (%)'
        } else if (input == 'LastPrice') {
            return 'Last Price'
        } else if (input == 'Change') {
            return 'Day Change'
        } else if (input == 'CustomSort') {
            return 'Custom Sort'
        } else {
            return input
        }
    };
}

tableHeaderFilter.$inject = []

export default tableHeaderFilter;
