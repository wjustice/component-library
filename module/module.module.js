import moduleDirective from './module.directive';

export default angular.module('module', [])
    .directive('module', moduleDirective)
