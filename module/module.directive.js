import template from "./module.template.html!text";
import _ from 'lodash';

function moduleDirective($timeout, $interval, $http, $compile, $rootScope, $document) {
    return {
        restrict: "E",
        scope: {
            module: '=',
            grid: '=',
            index: '='
        },
        template: template,
        link: function(scope, element) {

            let startX = 0,
                startY = 0,
                startWidth = 0,
                startHeight = 0,
                targetWidth = 0,
                targetHeight = 0,
                x = 0,
                y = 0,
                tolerance = 35;

            // scope.currentlyResizing = null;

            let content = element.find('.content'),
                widthControl = element.find('.width-control'),
                heightControl = element.find('.height-control'),
                sizeControl = element.find('.size-control'),
                resizePreview = element.find('.resize-preview');

            content.css({
                width: (scope.module.w - 30) + 'px',
                height: (scope.module.h - 30) + 'px'
            });

            let relayout = function() {
                $rootScope.$broadcast('relayout');
            }

            scope.deleteModule = function(module) {
                $rootScope.$broadcast('delete-module', module, scope.index);
            }

            // Trigger a relayout on the next digest cycle, just to make sure the module has mounted.
            $timeout(function() {
                relayout();
            }, 0)

            let startResize = function(event, type) {
                startX = event.pageX;
                startY = event.pageY;
                startWidth = content[0].offsetWidth + 10;
                startHeight = content[0].offsetHeight + 10;
                $document.on('mousemove', mousemove);
                $document.on('mouseup', mouseup);
                targetWidth = startWidth;
                targetHeight = startHeight;
            }

            widthControl.on('mousedown', function(event) {
                event.preventDefault();
                startResize(event);
                scope.currentlyResizing = 'w';
            });

            heightControl.on('mousedown', function(event) {
                event.preventDefault();
                startResize(event, 'h');
                scope.currentlyResizing = 'h';
            });

            sizeControl.on('mousedown', function(event) {
                event.preventDefault();
                startResize(event, 'wh');
                scope.currentlyResizing = 'wh';
            });

            function mousemove(event) {
                y = event.pageY - startY;
                x = event.pageX - startX;

                if (scope.currentlyResizing === 'w') {
                    widthLogic();
                } else if (scope.currentlyResizing === 'h') {
                    heightLogic();
                } else if (scope.currentlyResizing === 'wh') {
                    widthLogic();
                    heightLogic();
                }
            }

            function widthLogic() {
                content.css({
                    width: (startWidth + x - 30) + 'px'
                });

                resizePreview.css({
                    width: (targetWidth - 10) + 'px',
                    height: (targetHeight - 10) + 'px'
                })

                scope.module.w = (startWidth + x - 30);

                if (scope.module.w <= scope.module.minW) {
                    content.css({
                        width: (scope.module.minW - 30) + 'px'
                    });
                    targetWidth = scope.module.minW;
                    scope.module.w = scope.module.minW
                    scope.atMinW = true;
                } else if (scope.module.w + tolerance >= scope.module.maxW) {
                    content.css({
                        width: (scope.module.maxW - 30) + 'px'
                    });
                    targetWidth = scope.module.maxW;
                    scope.module.w = scope.module.maxW
                    scope.atMinW = true;
                } else {
                    content.css({
                        width: (startWidth + x - 30) + 'px'
                    });
                    scope.atMinW = false;
                    targetWidth = (Math.round((startWidth + x - tolerance) / scope.grid.columnWidth) * scope.grid.columnWidth) + scope.grid.columnWidth;
                    if (scope.currentlyResizing === 'w') {
                        targetHeight = (Math.round((startHeight - 30) / scope.grid.rowHeight) * scope.grid.rowHeight);
                    }

                    scope.module.w = (startWidth + x - 30);
                }

                relayout();
                scope.$apply();
            }

            function heightLogic() {
                content.css({
                    height: (startHeight + y - 30) + 'px'
                });

                scope.module.h = (startHeight + y - 30);

                resizePreview.css({
                    width: (targetWidth - 10) + 'px',
                    height: (targetHeight - 10) + 'px'
                });

                scope.module.h = (startHeight + y - 30);

                if (content[0].offsetHeight <= scope.module.minH) {
                    content.css({
                        height: (scope.module.minH - 30) + 'px'
                    });
                    targetHeight = scope.module.minH;
                    scope.module.h = scope.module.minH;
                    scope.atMinH = true;
                } else if (scope.module.h + tolerance >= scope.module.maxH) {
                    content.css({
                        height: (scope.module.maxH - 30) + 'px'
                    });
                    targetHeight = scope.module.maxH;
                    scope.module.h = scope.module.maxH;
                    scope.atMinH = true;
                } else {
                    content.css({
                        height: (startHeight + y - 30) + 'px'
                    });
                    scope.atMinH = false;
                    targetHeight = (Math.round((startHeight + y - tolerance) / scope.grid.rowHeight) * scope.grid.rowHeight) + scope.grid.rowHeight;
                    if (scope.currentlyResizing === 'h') {
                        targetWidth = (Math.round((startWidth - 30) / scope.grid.columnWidth) * scope.grid.columnWidth);
                    }
                    scope.module.h = (startHeight + y - 30);
                }

                relayout();
                scope.$apply();
            }

            function mouseup() {
                scope.currentlyResizing = null;
                scope.module.w = targetWidth;
                scope.module.h = targetHeight;
                scope.$apply();
                content.css({
                    width: (targetWidth - 30) + 'px',
                    height: (targetHeight - 30) + 'px'
                });

                relayout();
                $document.off('mousemove', mousemove);
                $document.off('mouseup', mouseup);
            }

            scope.$on('window-resize', function(event, width, height) {
                scope.module.maxW = Math.round((width - 30) / scope.grid.columnWidth) * scope.grid.columnWidth;
                if (scope.module.w + tolerance >= width) {
                    scope.module.w = scope.module.maxW;
                    content.css({
                        width: (scope.module.maxW - 30) + 'px'
                    });
                }
            });

            // let newElement = {};

            // if (scope.module.type == 'list') {
            //     let newTemplate = template.toString() + '<custom-table data="data.data" parentdimensions="data" data="masterDataObject" colorize="Change, ChangePercent, ChangePercentYTD, ChangeYTD"></custom-table>';
            //     newElement = $compile(newTemplate)(scope)
            // } else if (scope.module.type = 'chart') {
            //     // newElement = $compile('<div chart class="my-chart" style="width: {{module.w-10}}px; height: {{module.h-30}}px;" parentmodule="module"></div>')(scope)
            // }

            // // console.log(element.find('module-header'));

            // let hello = element.find('.content')[0];

            // element.append(newElement);
        }
    };
}

moduleDirective.$inject = ['$timeout', '$interval', '$http', '$compile', '$rootScope', '$document']

export default moduleDirective;
