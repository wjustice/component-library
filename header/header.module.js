import headerDirective from './header.directive';

export default angular.module('header', [])
    .directive('header', headerDirective)
