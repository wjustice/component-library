import template from "./header_directive.html!text";

function headerDirective() {
    return {
        restrict: "E",
        template: template,
        link: function(scope, element) {

        }
    };
}

headerDirective.$inject = []

export default headerDirective;
