import React from 'react';

class HelloWorld extends React.Component {
    render() {
        return <p>Hello, World! Reactify!</p>;
    }
}

export default HelloWorld;
