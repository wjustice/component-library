import template from "./lookup_directive.html!text";
import $ from 'jquery';

function lookupDirective($rootScope, $timeout) {
    return {
        restrict: 'AE',
        template: template,
        scope: {
            parentmodule: '='
        },
        link: function(scope, element, attrs) {
            scope.results = [];
            scope.input = {};

            scope.updateResults = function(input) {
                if (input != '') {
                    $.ajax({
                        url: "http://dev.markitondemand.com/api/v2/Lookup/jsonp",
                        dataType: "jsonp",
                        data: {
                            input: scope.input.requestTerm
                        },
                        success: function(data) {
                            $timeout(function() {
                                scope.results = data;
                            }, 0);
                        }
                    });
                }

            }

            scope.addSymbol = function(symbol) {
                scope.results = [];
                scope.input.requestTerm = '';
                $rootScope.$broadcast('add-symbol-from-lookup', symbol, scope.parentmodule.$$hashKey);
            };

            scope.clearResults = function() {
                $timeout(function() {
                    scope.results = [];
                    scope.input.requestTerm = '';
                }, 100);
            };
        }
    };
}

lookupDirective.$inject = ['$rootScope', '$timeout']

export default lookupDirective;
