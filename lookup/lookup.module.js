import lookupDirective from './lookup.directive';

export default angular.module('symbolLookup', [])
    .directive('symbolLookup', lookupDirective)
