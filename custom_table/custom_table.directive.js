import template from "./custom_table_directive.html!text";
import _ from 'lodash';

function customTableDirective($timeout, $rootScope, $document) {
    return {
        restrict: 'AE',
        template: template,
        scope: {
            model: '=',
            parentdimensions: '=',
            colorize: '@',
            grid: '='
        },
        link: function(scope, element, attrs) {

            scope.addChart = function(type, data) {
                $rootScope.$broadcast('add-chart-from-list', type, data);
            };

            scope.breakpoint = null;

            var scrollOffset = 0,
                tempX = 0,
                tempY = 0,
                scrollAmount = 0,
                origin = {},
                newScrollAmount = 0;

            var containerHeight = element[0].parentNode.offsetHeight,
                table = element.find('.main-table'),
                tbody = table.find('tbody')[0],
                thead = table.find('thead')[0],
                minimap = element.find('.mini-map'),
                miniTable = minimap.find('table')[0],
                miniTbody = minimap.find('tbody')[0],
                scrollHighlight = minimap.find('.scroll-highlight')[0],
                scrollRatio = tbody.offsetHeight / tbody.scrollHeight,
                tr = table.find('tr')[0];

            let setSize = function() {
                scope.parentdimensions.maxH = Math.round((tr.offsetHeight * scope.model.length + 70) / scope.grid.rowHeight) * scope.grid.rowHeight + scope.grid.rowHeight;

                tbody.style.height = scope.parentdimensions.h - thead.offsetHeight - 50 + 'px';

                if (scope.parentdimensions.h >= tbody.scrollHeight + 70) {
                    minimap[0].style.width = '0px';
                    table[0].style.width = scope.parentdimensions.w - 30 + 'px';
                } else {
                    minimap[0].style.width = '20px';
                    miniTbody.style.height = tbody.offsetHeight + 'px';
                    miniTable.style.height = tbody.offsetHeight + 'px';

                    // reset the scroll ratio
                    scrollRatio = tbody.offsetHeight / tbody.scrollHeight;

                    scrollHighlight.style.height = tbody.offsetHeight * scrollRatio + 'px';
                    scrollHighlight.style.top = thead.offsetHeight + 40 + 'px';
                    miniTable.style.top = thead.offsetHeight + 40 + 'px';
                    table[0].style.width = scope.parentdimensions.w - 70 + 'px';
                }
            }

            function mousemove(e) {
                tempX = e.pageX;
                tempY = e.pageY;

                if (tempX <= 0) {
                    tempX = 0;
                }
                if (tempY <= 0) {
                    tempY = 0;
                }

                var mouseDelta = tempY - origin.y

                newScrollAmount = scrollAmount + mouseDelta;

                if (newScrollAmount <= 0) {
                    newScrollAmount = 0;
                }
                if (newScrollAmount >= (tbody.offsetHeight - scrollHighlight.offsetHeight)) {
                    newScrollAmount = tbody.offsetHeight - scrollHighlight.offsetHeight;
                }
                scrollHighlight.style.transform = 'translateY(' + newScrollAmount + 'px)'
                tbody.scrollTop = newScrollAmount / scrollRatio;

                return true;
            }

            function startScroll(e) {
                origin.x = e.pageX;
                origin.y = e.pageY;
                $document.on('mousemove', mousemove);
                $document.on('mouseup', stopScroll);
            }

            function stopScroll() {
                scrollAmount = newScrollAmount;
                $document.off('mouseup', stopScroll);
                $document.off('mousemove', mousemove);
            }

            function tableScroll() {
                scrollHighlight.style.transform = 'translateY(' + (tbody.scrollTop * scrollRatio) + 'px)'
            }

            minimap.find('.scroll-highlight').on('mousedown', startScroll);
            table.find('tbody').on('scroll', tableScroll);

            scope.$watch('parentdimensions.w', function(newValue, oldValue) {
                if (newValue) {
                    setSize();
                    if (scope.parentdimensions.w <= 800) {
                        scope.breakpoint = 'small-break';
                    } else {
                        scope.breakpoint = null;
                    }
                }
            }, true);

            scope.$watch('parentdimensions.h', function(newValue, oldValue) {
                if (newValue) {
                    setSize();
                }
            }, true);

            // set size on the next digest after the directive is instantiated
            $timeout(function() {
                setSize();
            }, 100)

            scope.checkPositiveNegative = function(key, val) {
                if (scope.colorize.indexOf(key) > -1) {
                    if (val >= 0) {
                        // console.log('running checkPositiveNegative...');
                        return 'green'
                    } else if (val < 0) {
                        return 'red'
                    }
                }
            }

            scope.sort = function(val) {
                // console.log('running sort...');
                if (scope.table.order == val) {
                    scope.table.order = ('-' + val);
                } else {
                    scope.table.order = val;
                }
            };
        }
    };
}

customTableDirective.$inject = ['$timeout', '$rootScope', '$document']

export default customTableDirective;
