import customTableDirective from './custom_table.directive';

export default angular.module('customTable', [])
    .directive('customTable', customTableDirective)
