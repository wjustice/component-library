import template from './topNav.template.html!text';

function topNavDirective() {
    return {
        restrict: 'E',
        template: template,
        link: function(scope, element) {}
    };
}
topNavDirective.$inject = []
export default topNavDirective;
