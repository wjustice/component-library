function keypressEventsDirective($rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element) {

            let handler = function(e) {
                if (e.keyCode === 39) {
                    // console.log('right arrow');
                    e.preventDefault();
                    $rootScope.$broadcast('keypress', 39);
                } else if (e.keyCode === 37) {
                    // console.log('left arrow');
                    e.preventDefault();
                    $rootScope.$broadcast('keypress', 37);
                } else if (e.keyCode === 27) {
                    // console.log('escape');
                    e.preventDefault();
                    $rootScope.$broadcast('keypress', 27);
                } else if (e.keyCode === 13) {
                    console.log('enter');
                    e.preventDefault();
                    $rootScope.$broadcast('keypress', 13);
                } else if (e.keyCode === 16) {
                    console.log('shift down');
                    e.preventDefault();
                }
            };

            let $doc = angular.element(document);

            $doc.on('keydown', handler);

            // $doc.on('keyup', function(e) {
            //     if (e.keyCode === 16) {
            //         console.log('shift up');
            //     }
            // })

        }
    };
}
keypressEventsDirective.$inject = ['$rootScope']
export default keypressEventsDirective;
