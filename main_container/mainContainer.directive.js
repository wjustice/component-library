import template from './mainContainer.template.html!text';
import React from 'react';
import ReactDOM from 'react-dom';
import HelloWorld from '../react_home/home';

function mainContainerDirective() {
    return {
        restrict: 'E',
        template: template,
        link: function(scope, element) {

            ReactDOM.render(
                <HelloWorld />,
                element.find('#editor')[0]
            );

        }
    };
}

mainContainerDirective.$inject = []
export default mainContainerDirective;
