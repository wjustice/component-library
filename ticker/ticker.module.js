import tickerDirective from './ticker.directive';

export default angular.module('ticker', [])
    .directive('ticker', tickerDirective)
