import template from "./ticker_directive.html!text";
import _ from "lodash";

function tickerDirective($timeout, $interval, $window, $rootScope) {
    return {
        restrict: "E",
        scope: {
            model: '='
        },
        template: template,
        link: function(scope, element) {

            var container = {};
            var tickerItem = {};
            var visibleItemCount = 0;

            scope.slideToggle = true;
            var slideInterval;

            let roundToTwo = function(x) {
                return Math.round(x * 100) / 100;
            };

            function doStuff() {
                scope.allLists = scope.model;
                container = element.find('.ticker-container')[0];

                scope.allLists.map(x => x.averageChange = roundToTwo(_.random(-2, 2, true)))

                $timeout(function() {
                    tickerItem = element.find('#0');
                    visibleItemCount = Math.floor(container.offsetWidth / tickerItem[0].offsetWidth);

                }, 0)
            }

            scope.start = function() {
                if (visibleItemCount < scope.allLists.length) {
                    scope.stop();
                    scope.slideToggle = true;
                    slideInterval = $interval(function() {
                        scope.allLists.push(scope.allLists.shift())
                    }, 2000)
                } else {
                    scope.stop();
                }
            }

            scope.stop = function() {
                $interval.cancel(slideInterval);
                scope.slideToggle = false;
            }

            scope.$watch('model', function(newValue, oldValue) {
                if (newValue) {
                    doStuff();
                }
            })

            scope.addModule = function(list) {
                $rootScope.$broadcast('add-list-module', list);

            }

            scope.checkPositiveNegative = function(val) {
                if (val >= 0) {
                    return 'green'
                } else if (val < 0) {
                    return 'red'
                }
            }

            var thisWindow = angular.element($window);
            scope.getWindowDimensions = function() {
                return {
                    'h': thisWindow.height(),
                    'w': thisWindow.width()
                };
            };

            scope.$watch(scope.getWindowDimensions, function(newValue, oldValue) {
                scope.windowHeight = newValue.h;
                scope.windowWidth = newValue.w;

                $timeout(function() {
                    container = element.find('.ticker-container')[0];
                    tickerItem = element.find('#0');

                    visibleItemCount = Math.floor(container.offsetWidth / tickerItem[0].offsetWidth);

                    if (visibleItemCount < scope.allLists.length) {
                        scope.start();
                    } else {
                        scope.stop();
                    }
                }, 0)

            }, true);

            // thisWindow.bind('resize', function() {
            //     scope.$apply();
            // });

        }
    };
}

tickerDirective.$inject = ['$timeout', '$interval', '$window', '$rootScope']

export default tickerDirective;
